﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Cnc.Payroll.InterviewTest.Domain.Response;
using Cnc.Payroll.InterviewTest.Domain;
using Cnc.Payroll.InterviewTest.Domain.Request;

namespace Cnc.Payroll.InterviewTest.Controllers
{
    [ApiExplorerSettings(IgnoreApi=true)]
    [ApiController]
    [Route("[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly ILogger<ProjectController> _logger;

        public ProjectController(ILogger<ProjectController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Gets a project
        /// </summary>
        /// <param name="request">Get Project request contains project number.</param>
        /// <returns>ActionResult with Project</returns>
        [HttpPost]
        public GetProjectResponse Get(GetProjectRequest request)
        {
            return new GetProjectResponse
            {
                Data = new Project { ProjectNumber = 20}
            };
        }
    }
}
