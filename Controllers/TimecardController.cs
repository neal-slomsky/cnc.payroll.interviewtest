﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cnc.Payroll.InterviewTest.Domain;
using Cnc.Payroll.InterviewTest.Domain.Request;
using Cnc.Payroll.InterviewTest.Domain.Response;
using Cnc.Payroll.InterviewTest.Service.Contract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Cnc.Payroll.InterviewTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TimecardController : ControllerBase
    {

        private readonly ILogger<TimecardController> _logger;
        private readonly ITimecardService  _timecardService;

        public TimecardController(ILogger<TimecardController> logger,
            ITimecardService timecardService)
        {
            _logger = logger;
            _timecardService = timecardService;
        }

        /// <summary>
        /// Gets Timecards
        /// </summary>
        /// <param name="request">Timecard request contains timecard number.</param>
        /// <returns>ActionResult with requested timecard and result status</returns>
        [HttpPost("get")]
        public List<Timecard> GetTimecards(GetTimecardRequest request)
        {
            return _timecardService.GetTimecards(10, 11);
        }

        /// <summary>
        /// Gets Timecards grouped by Status
        /// </summary>
        /// <param name="request">Timecard request contains timecard number.</param>
        /// <returns>ActionResult with requested timecard and result status</returns>
        [HttpPost("getTimecardsBasedOnStatus")]
        public GetTimecardsBrokenOutByStatusResponse GetTimecardsBrokenOutByStatus(GetTimecardRequest request)
        {
            return _timecardService.GetTimecardsBrokenOutByStatus(request.ProductionCompany, request.ProjectNumber);
        }
    }
}
