using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cnc.Payroll.InterviewTest.Domain.Request
{
    public class GetProjectRequest
    {
        public int ProjectNumber { get; set; }
    }
}