﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cnc.Payroll.InterviewTest.Domain.Request
{
    public class GetTimecardRequest
    {
        public int ProductionCompany { get; set; }

        public int ProjectNumber { get; set; }
    }
}
