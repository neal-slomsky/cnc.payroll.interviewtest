using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cnc.Payroll.InterviewTest.Domain.Response
{
    public class GetProjectResponse
    {
        public Project Data {get; set;}
        
    }
}