﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cnc.Payroll.InterviewTest.Domain.Response
{
    public class GetTimecardsBrokenOutByStatusResponse
    {
        public TimecardsByStatus Data {get; set; } 
    }

    public class TimecardsByStatus
    {
        public TimecardsByStatus()
        {
            TimecardsTrue = new List<Timecard>();
            TimecardsFalse = new List<Timecard>();
        }

        public List<Timecard> TimecardsTrue { get; set; }

        public List<Timecard> TimecardsFalse { get; set; }
    }
}
