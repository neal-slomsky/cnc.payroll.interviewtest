﻿using System;

namespace Cnc.Payroll.InterviewTest.Domain
{
    public class Timecard
    {

        public long TimecardNumber { get; set; }

        public int? InvoiceNumber { get; set; }

        public int? BatchNumber { get; set; }

        public bool Status { get; set; }

        public int? ProductionCompany { get; set; }

        public int? ProjectNumber { get; set; }

        public DateTime? DateEntered { get; set; }
    }
}
