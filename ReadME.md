1. Fix Runtime error regarding dependency injection.
2. The business users have asked to make the Projects controller not visible when viewed through swagger.
3. The business users have asked for a change in the TimecardController method getTimecardsBasedOnStatus.
    They would like both the Production Company and Prject Number required.
4. The business users would like the Timecard controller method GetTimecardsBrokenOutByStatus to return a response
   object which contains two list of timecards:
    a. The first list named TimecardsTrue would contain all Timecards where Status equals true and DateEntered
       has a value, please order by DateEntered descending.
    b. The second list named TimecardsFalse would contain all Timecards where Status equals false and DateEntered
       does not have a value, please order by InvoiceNumber ascending.
5. Thats It!