﻿using Cnc.Payroll.InterviewTest.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cnc.Payroll.InterviewTest.Repository.Contract
{
    public interface ITimecardRepository
    {

        public List<Timecard> GetTimecards(int ProductionCompany, int ProjectNumber);

        public List<Timecard> GetTimecardsBrokenOutByStatus(int ProductionCompany, int ProjectNumber);
    }
}
