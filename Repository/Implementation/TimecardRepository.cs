﻿using Cnc.Payroll.InterviewTest.Domain;
using Cnc.Payroll.InterviewTest.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cnc.Payroll.InterviewTest.Repository.Implementation
{
    public class TimecardRepository : ITimecardRepository
    {

        private List<Timecard> timecardDataStore = new List<Timecard>
        { 
            new Timecard {TimecardNumber = 500, BatchNumber = 10,InvoiceNumber=111,Status=true,DateEntered = DateTime.UtcNow.AddDays(-20), ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 501, BatchNumber = 10,InvoiceNumber=112,Status=true,DateEntered = null, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 502, BatchNumber = 10,InvoiceNumber=113,Status=true,DateEntered = DateTime.UtcNow, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 503, BatchNumber = 10,InvoiceNumber=114,Status=true,DateEntered = DateTime.UtcNow, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 504, BatchNumber = 10,InvoiceNumber=115,Status=false,DateEntered = null, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 505, BatchNumber = 10,InvoiceNumber=116,Status=true,DateEntered = DateTime.UtcNow, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 506, BatchNumber = 10,InvoiceNumber=117,Status=true,DateEntered = DateTime.UtcNow, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 507, BatchNumber = 10,InvoiceNumber=118,Status=true,DateEntered = DateTime.UtcNow.AddDays(-2), ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 508, BatchNumber = 10,InvoiceNumber=119,Status=false,DateEntered = null, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 509, BatchNumber = 10,InvoiceNumber=120,Status=true,DateEntered =null, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 510, BatchNumber = 10,InvoiceNumber=121,Status=false,DateEntered = DateTime.UtcNow.AddDays(-6), ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 511, BatchNumber = 10,InvoiceNumber=122,Status=false,DateEntered = DateTime.UtcNow.AddDays(-7), ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 512, BatchNumber = 10,InvoiceNumber=123,Status=false,DateEntered = DateTime.UtcNow, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 513, BatchNumber = 10,InvoiceNumber=124,Status=false,DateEntered = DateTime.UtcNow, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 514, BatchNumber = 10,InvoiceNumber=125,Status=true,DateEntered = DateTime.UtcNow, ProductionCompany = 10,ProjectNumber= 12},
            new Timecard {TimecardNumber = 515, BatchNumber = 10,InvoiceNumber=126,Status=false,DateEntered = DateTime.UtcNow.AddDays(8), ProductionCompany = 10,ProjectNumber= 12},

            new Timecard {TimecardNumber = 600, BatchNumber = 10,InvoiceNumber=125,Status=true,DateEntered = null, ProductionCompany = 100,ProjectNumber= 200},
            new Timecard {TimecardNumber = 601, BatchNumber = 10,InvoiceNumber=126,Status=false,DateEntered = DateTime.UtcNow.AddDays(8), ProductionCompany = 100,ProjectNumber= 200},
            new Timecard {TimecardNumber = 602, BatchNumber = 10,InvoiceNumber=125,Status=true,DateEntered = DateTime.UtcNow, ProductionCompany = 100,ProjectNumber= 200},
            new Timecard {TimecardNumber = 603, BatchNumber = 10,InvoiceNumber=126,Status=false,DateEntered = null, ProductionCompany = 100,ProjectNumber= 200},

            new Timecard {TimecardNumber = 700, BatchNumber = 10,InvoiceNumber=125,Status=true,DateEntered = null, ProductionCompany = 400,ProjectNumber= 500},
            new Timecard {TimecardNumber = 701, BatchNumber = 10,InvoiceNumber=126,Status=false,DateEntered = DateTime.UtcNow.AddDays(18), ProductionCompany = 400,ProjectNumber= 500},
            new Timecard {TimecardNumber = 702, BatchNumber = 10,InvoiceNumber=125,Status=true,DateEntered = DateTime.UtcNow, ProductionCompany = 400,ProjectNumber= 500},
            new Timecard {TimecardNumber = 703, BatchNumber = 10,InvoiceNumber=126,Status=false,DateEntered = DateTime.UtcNow.AddDays(-5), ProductionCompany = 400,ProjectNumber= 500},
        };


        public List<Timecard> GetTimecards(int ProductionCompany, int ProjectNumber)
        {
            return timecardDataStore;
        }

        public List<Timecard> GetTimecardsBrokenOutByStatus(int ProductionCompany, int ProjectNumber)
        {
            return timecardDataStore.Where(x => x.ProductionCompany == ProductionCompany && x.ProjectNumber == ProjectNumber).ToList();
        }
    }
}
