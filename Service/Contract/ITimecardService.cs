﻿using Cnc.Payroll.InterviewTest.Domain;
using Cnc.Payroll.InterviewTest.Domain.Response;
using System.Collections.Generic;

namespace Cnc.Payroll.InterviewTest.Service.Contract
{
    public interface ITimecardService
    {

        public List<Timecard> GetTimecards(int ProductionCompany, int ProjectNumber);

        public GetTimecardsBrokenOutByStatusResponse GetTimecardsBrokenOutByStatus(int ProductionCompany, int ProjectNumber);
    }
}
