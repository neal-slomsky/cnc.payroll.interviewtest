﻿using Cnc.Payroll.InterviewTest.Domain;
using Cnc.Payroll.InterviewTest.Domain.Response;
using Cnc.Payroll.InterviewTest.Repository.Contract;
using Cnc.Payroll.InterviewTest.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cnc.Payroll.InterviewTest.Service.Implementation
{
    public class TimecardService : ITimecardService
    {
        private readonly ITimecardRepository _timecardRepository = null;


        public TimecardService(ITimecardRepository timecardRepository)
        {
            _timecardRepository = timecardRepository;
        }
        public List<Timecard> GetTimecards(int ProductionCompany, int ProjectNumber)
        {
            return _timecardRepository.GetTimecards(ProductionCompany, ProjectNumber);
        }

        public GetTimecardsBrokenOutByStatusResponse GetTimecardsBrokenOutByStatus(int ProductionCompany, int ProjectNumber)
        {
            var allTimecards = _timecardRepository.GetTimecardsBrokenOutByStatus(ProductionCompany, ProjectNumber);

            return new GetTimecardsBrokenOutByStatusResponse()
            {
                Data = new TimecardsByStatus
                {
                    TimecardsTrue = null,
                    TimecardsFalse = null,
                }
            };
        }
    }
}
